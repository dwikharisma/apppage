-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `detail_project`;
CREATE TABLE `detail_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `file` varchar(225) NOT NULL,
  `link` varchar(225) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `tag_h1` int(11) NOT NULL,
  `tag_h1_match` int(11) NOT NULL,
  `tag_h2` int(11) NOT NULL,
  `tag_h2_match` int(11) NOT NULL,
  `tag_h3` int(11) NOT NULL,
  `tag_h3_match` int(11) NOT NULL,
  `tag_h4` int(11) NOT NULL,
  `tag_h4_match` int(11) NOT NULL,
  `tag_h5` int(11) NOT NULL,
  `tag_h5_match` int(11) NOT NULL,
  `tag_h6` int(11) NOT NULL,
  `tag_h6_match` int(11) NOT NULL,
  `tag_anchor` int(11) NOT NULL,
  `tag_paragraph` int(11) NOT NULL,
  `tag_paragraph_match` int(11) NOT NULL,
  `tag_bold` int(11) NOT NULL,
  `tag_italic` int(11) NOT NULL,
  `tag_img_file` int(11) NOT NULL,
  `tag_img_alt` int(11) NOT NULL,
  `tag_ordered` int(11) NOT NULL,
  `tag_ordered_list` int(11) NOT NULL,
  `tag_unordered` int(11) NOT NULL,
  `tag_unordered_list` int(11) NOT NULL,
  `tag_table` int(11) NOT NULL,
  `tag_table_item` int(11) NOT NULL,
  `form_total` int(11) NOT NULL,
  `words` int(11) NOT NULL,
  `words_match` int(11) NOT NULL,
  `detail` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `detail_project_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `project` varchar(225) NOT NULL,
  `keywords` varchar(225) NOT NULL,
  `url` varchar(225) NOT NULL,
  `file` varchar(225) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-12-12 08:09:55
