<?php 
class Appmodel extends CI_Model {

	function new_project($file){
		$data['name']=$this->input->post('name');
		$data['url']=$this->input->post('url');
		$data['project']=$file;
		$data['keywords']=$this->input->post('keywords');
		$data['file']=$file.'.html';
		$data['timestamp']=date('Y-m-d H:i:s',time());
		$this->db->insert('project',$data);
		return $this->db->insert_id();
	}

	function parse($data){
		$in['project_id']=$data['id'];
		$in['file']=$data['file'];
		$in['type']=$data['type'];
		$in['link']=$data['url'];
		$in['tag_h1']=$data['parse']['tag_h1'];
		$in['tag_h1_match']=$data['parse']['tag_h1_match_nums'];
		$in['tag_h2']=$data['parse']['tag_h2'];
		$in['tag_h2_match']=$data['parse']['tag_h2_match_nums'];
		$in['tag_h3']=$data['parse']['tag_h3'];
		$in['tag_h3_match']=$data['parse']['tag_h3_match_nums'];
		$in['tag_h4']=$data['parse']['tag_h4'];
		$in['tag_h4_match']=$data['parse']['tag_h4_match_nums'];
		$in['tag_h5']=$data['parse']['tag_h5'];
		$in['tag_h5_match']=$data['parse']['tag_h5_match_nums'];
		$in['tag_h6']=$data['parse']['tag_h6'];
		$in['tag_h6_match']=$data['parse']['tag_h6_match_nums'];

		$in['tag_anchor']=$data['parse']['tag_a'];
		$in['tag_paragraph']=$data['parse']['tag_p'];
		$in['tag_paragraph_match']=$data['parse']['tag_p_match_nums'];
		$in['tag_bold']=$data['parse']['tag_b'];
		$in['tag_italic']=$data['parse']['tag_i'];
		$in['tag_img_file']=$data['parse']['tag_img_src_num'];
		$in['tag_img_alt']=$data['parse']['tag_img_alt_num'];
		$in['tag_ordered']=$data['parse']['tag_ol_num'];
		$in['tag_ordered_list']=$data['parse']['tag_ol_li_num'];
		$in['tag_unordered']=$data['parse']['tag_ul_num'];
		$in['tag_unordered_list']=$data['parse']['tag_ul_li_num'];
		$in['words']=$data['parse']['txt_words_nums'];
		$in['words_match']=$data['parse']['txt_words_match'];
		$in['detail']=json_encode($data['parse']);
		return $this->db->insert('detail_project',$in);

	}

	function project($id){
		return $this->db->where('project_id',$id)->where('type',1)->get('detail_project')->row();
	}

	function project_dt($id){
		return $this->db->where('id',$id)->get('project')->row();
	}

	function perform_sum($id){
		$this->db->select_sum('tag_h1');
		$this->db->select_sum('tag_h1_match');
		$this->db->select_sum('tag_h2');
		$this->db->select_sum('tag_h2_match');
		$this->db->select_sum('tag_h3');
		$this->db->select_sum('tag_h3_match');
		$this->db->select_sum('tag_h4');
		$this->db->select_sum('tag_h4_match');
		$this->db->select_sum('tag_h5');
		$this->db->select_sum('tag_h5_match');
		$this->db->select_sum('tag_h6');
		$this->db->select_sum('tag_h6_match');

		$this->db->select_sum('tag_anchor');
		$this->db->select_sum('tag_paragraph');
		$this->db->select_sum('tag_paragraph_match');
		$this->db->select_sum('tag_bold');
		$this->db->select_sum('tag_italic');
		$this->db->select_sum('tag_img_file');
		$this->db->select_sum('tag_img_alt');
		$this->db->select_sum('tag_ordered');
		$this->db->select_sum('tag_ordered_list');
		$this->db->select_sum('tag_unordered');
		$this->db->select_sum('tag_unordered_list');
		$this->db->select_sum('tag_unordered_list');
		$this->db->select_sum('words');
		$this->db->select_sum('words_match');
		$this->db->where('project_id',$id);
		$this->db->where('type',0);

		$query = $this->db->get('detail_project')->row(); // Produces: SELECT SUM(age) as age FROM members
		return $query;
	}


	function perform_avg($id){
		$this->db->select_avg('tag_h1');
		$this->db->select_avg('tag_h1_match');
		$this->db->select_avg('tag_h2');
		$this->db->select_avg('tag_h2_match');
		$this->db->select_avg('tag_h3');
		$this->db->select_avg('tag_h3_match');
		$this->db->select_avg('tag_h4');
		$this->db->select_avg('tag_h4_match');
		$this->db->select_avg('tag_h5');
		$this->db->select_avg('tag_h5_match');
		$this->db->select_avg('tag_h6');
		$this->db->select_avg('tag_h6_match');
		$this->db->select_avg('tag_anchor');
		$this->db->select_avg('tag_paragraph');
		$this->db->select_avg('tag_paragraph_match');
		$this->db->select_avg('tag_bold');
		$this->db->select_avg('tag_italic');
		$this->db->select_avg('tag_img_file');
		$this->db->select_avg('tag_img_alt');
		$this->db->select_avg('tag_ordered');
		$this->db->select_avg('tag_ordered_list');
		$this->db->select_avg('tag_unordered');
		$this->db->select_avg('tag_unordered_list');
		$this->db->select_avg('tag_unordered_list');
		$this->db->select_avg('words');
		$this->db->select_avg('words_match');
		$this->db->where('project_id',$id);
		$this->db->where('type',0);

		$query = $this->db->get('detail_project')->row(); // Produces: SELECT SUM(age) as age FROM members
		return $query;
	}


	/*list media*/
	function list($number=1, $offset=1){
		$data['nums']=$this->db->get('project')->num_rows();
		$data['data']=$this->db->limit($offset,$number)->order_by('id','desc')->get('project') ->result();
		return $data;
	}

}