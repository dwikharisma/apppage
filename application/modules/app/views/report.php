<div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Project</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Priject</a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li class="active">Basic</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">       
            <div class="row">
            <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Report</strong>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Project Name</th>
                                          <th scope="col">Keyword</th>
                                          <th scope="col">Url</th>
                                          <th scope="col">__</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach ($data as $key => $value) {?>
                                        <tr>
                                        <th scope="row"><?php echo $key+1; ?></th>
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->keywords; ?></td>
                                        <td><?php echo $value->url; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-primary mb-1 btn-detail" data-toggle="modal" data-target="#staticModal" data-id="<?php echo $value->id; ?>">
                                              Detail
                                          </button>
                                      </td>
                                    </tr>
                                    <?php } ?>                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
            <div class="clearfix"></div>
    <footer class="site-footer">
        <div class="footer-inner bg-white">
            <div class="row">
                <div class="col-sm-6">
                    Copyright &copy; 2018 Ela Admin
                </div>
                <div class="col-sm-6 text-right">
                    Designed by <a href="https://colorlib.com">Colorlib</a>
                </div>
            </div>
        </div>
    </footer>

</div><!-- /#right-panel -->

<!-- Right Panel -->
<div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">Detail Laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
        </div>
    </div>
</div>
        </div>
<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/main.js"></script>
<script>
     jQuery(document).ready(function($) {

    $(".btn-detail").click(function(e) {
     e.preventDefault();
     var trid=$(this).attr('data-id');
     
       $.ajax({
            url: '/app/analyze', //this is the submit URL
            type: 'POST', //or POST
            data: {project_id:trid},
            'beforeSend': function(){
            $.blockUI({ css: { backgroundColor: '', color: '',border:'0px' },message: '<img src="<?php echo base_url();?>images/facebook.gif">' });
             //$('.add-pro').fadeOut();             
              },
              error: function (jqXHR, textStatus, errorThrown){
              } ,
            success: function(response){
                $.unblockUI();
                console.log(response);
                $('.modal-body').html(response.html);
                // $('#project_keywords').val(response.keywords);
                
                
            }
        });
   });
});
   


</script>



</body>
</html>
