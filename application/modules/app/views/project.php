<div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Project</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Priject</a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li class="active">Basic</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Add Address</strong> Project
                            </div>
                            <form class="form-horizontal" id="f-project">
                                <input type="hidden" name="modul" value="new">
                            <div class="card-body card-block"> 
                            
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="hf-email" class=" form-control-label">Name</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="hf-email" name="name" placeholder="Enter Project Name" class="form-control"  required="true">
                                        </div>
                                    </div>   
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="hf-email" class=" form-control-label">Keywords</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="hf-email" name="keywords" placeholder="Enter Project Name" class="form-control"  required="true">
                                        </div>
                                    </div>   
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="hf-email" class=" form-control-label">Link Address</label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="hf-email" name="url" placeholder="http://travelingyuk.com" class="form-control"  required="true">
                                        </div>
                                    </div>                                    
                               
                            </div>
                            <div class="card-footer add-pro">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>
                             </form>
                        </div>
                    </div><!--/.col-->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Add Address</strong> Compare
                            </div>
                            <form  class="form-horizontal" id="compare-project">
                            <input type="hidden" name="project_id" id="project_id" class="project_id">    
                            <input type="hidden" name="project_keywords" id="project_keywords">    
                            <div class="card-body card-block">
                                
                                    <?php for($i=0;$i<5;$i++){?>

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="hf-email" class=" form-control-label">Link Address <?php echo $i+1;?></label></div>
                                        <div class="col-12 col-md-9"><input type="text" id="hf-email" name="url_<?php echo $i;?>" placeholder="http://travelingyuk.com" class="form-control"  required="true"></div>
                                    </div>
                                    <?php } ?>
                                                                    
                                
                            </div>
                            <div class="card-footer add-other">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </button>
                            </div>
                            </form>
                        </div>
                    </div><!--/.col-->

                     <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>Analyze</strong> 
                            </div>
                            <div class="card-body card-block">
                                <div id="result"></div>
                               <form  class="form-horizontal" id="analyze-project" style="display: none;">
                                 <input type="text" name="project_id" id="project_id" class="project_id">
                                <button type="submit" class="btn btn-primary btn-block btn-analyze">
                                    <i class="fa fa-dot-circle-o"></i> Analyze
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>            
        </div><!-- .animated -->
    </div><!-- .content -->
<div class="clearfix"></div>
    <footer class="site-footer">
        <div class="footer-inner bg-white">
            <div class="row">
                <div class="col-sm-6">
                    Copyright &copy; 2018 Ela Admin
                </div>
                <div class="col-sm-6 text-right">
                    Designed by <a href="https://colorlib.com">Colorlib</a>
                </div>
            </div>
        </div>
    </footer>

</div><!-- /#right-panel -->

<!-- Right Panel -->

<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/main.js"></script>
<script>
    console.log('test');
    jQuery(document).ready(function($) {
    $("#f-project").submit(function(e) {
     e.preventDefault();
       $.ajax({
            url: '/app/action', //this is the submit URL
            type: 'POST', //or POST
            data: $('#f-project').serialize(),
            'beforeSend': function(){
            $.blockUI({ css: { backgroundColor: '', color: '',border:'0px' },message: '<img src="<?php echo base_url();?>images/facebook.gif">' });
             $('.add-pro').fadeOut();             
              },
              error: function (jqXHR, textStatus, errorThrown){
              } ,
            success: function(response){
                $.unblockUI();
                console.log(response);
                $('.project_id').val(response.project);
                $('#project_keywords').val(response.keywords);
                
                
            }
        });
   // }
});

    $("#compare-project").submit(function(e) {
     e.preventDefault();
     console.log('hi');
       $.ajax({
            url: '/app/compare', //this is the submit URL
            type: 'POST', //or POST
            data: $('#compare-project').serialize(),
            'beforeSend': function(){
            $.blockUI({ css: { backgroundColor: '', color: '',border:'0px' },message: '<img src="<?php echo base_url();?>images/facebook.gif">' });
             $('.add-other').fadeOut();             
              },
              error: function (jqXHR, textStatus, errorThrown){
              } ,
            success: function(response){
                $.unblockUI();
                $('#analyze-project').show();
                console.log(response);
            }
        });
   // }
});

        $("#analyze-project").submit(function(e) {
     e.preventDefault();
       $.ajax({
            url: '/app/analyze', //this is the submit URL
            type: 'POST', //or POST
            data: $('#analyze-project').serialize(),
            'beforeSend': function(){
            $.blockUI({ css: { backgroundColor: '', color: '',border:'0px' },message: '<img src="<?php echo base_url();?>images/facebook.gif">' });
              $('#analyze-project').hide();             
              },
              error: function (jqXHR, textStatus, errorThrown){
              } ,
            success: function(response){
                $.unblockUI();
                $('#result').html(response.html);
            }
        });
   // }
});
});
</script>

</body>
</html>
