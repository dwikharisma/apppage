<div class="table-responsive">
<table class="table table-stripped">
	<thead>
		<tr>
			<th colspan="6" ><div align="center"><?php echo strtoupper($pro->name); ?></div></th>
			
		</tr>
		<tr>
			<th>No</th>
			<th>Tag</th>
			<th>Summary</th>
			<th>Average</th>
			<th>Current</th>
			<th>Description</th>
		</tr>
	</thead>
		<tr>
			<td>1</td>
			<td>Tag H1</td>
			<td><?php echo $sum->tag_h1; ?></td>
			<td><?php echo number_format((float)$avg->tag_h1, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h1; ?></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Tag H1 Keyword</td>
			<td><?php echo $sum->tag_h1_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h1_match, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h1_match; ?></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Tag H2</td>
			<td><?php echo $sum->tag_h2; ?></td>
			<td><?php echo number_format((float)$avg->tag_h2, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h2; ?></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Tag H2 Keyword</td>
			<td><?php echo $sum->tag_h2_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h2_match, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h2_match; ?></td>
		</tr>
		<tr>
			<td>5</td>
			<td>Tag H3</td>
			<td><?php echo $sum->tag_h3; ?></td>
			<td><?php echo number_format((float)$avg->tag_h3, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h3; ?></td>
		</tr>
		<tr>
			<td>6</td>
			<td>Tag H3 Keyword</td>
			<td><?php echo $sum->tag_h3_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h3_match, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h3_match; ?></td>
		</tr>
		<tr>
			<td>6</td>
			<td>Tag H4</td>
			<td><?php echo $sum->tag_h4; ?></td>
			<td><?php echo number_format((float)$avg->tag_h4, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h4; ?></td>
		</tr>
		<tr>
			<td>7</td>
			<td>Tag H4 Keyword</td>
			<td><?php echo $sum->tag_h4_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h4_match, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h4_match; ?></td>
		</tr>
		<tr>
			<td>8</td>
			<td>Tag H5</td>
			<td><?php echo $sum->tag_h5; ?></td>
			<td><?php echo number_format((float)$avg->tag_h5, 2, '.', ''); ?></td>
			<td><?php echo $cur->tag_h5; ?></td>
		</tr>
		<tr>
			<td>9</td>
			<td>Tag H5 Keyword</td>
			<td><?php echo $sum->tag_h5_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h5_match, 2, '.', ''); ?></td>			
			<td><?php echo $cur->tag_h5_match; ?></td>
		</tr>
		<tr>
			<td>10</td>
			<td>Tag H6</td>
			<td><?php echo $sum->tag_h6; ?></td>
			<td><?php echo number_format((float)$avg->tag_h6, 2, '.', ''); ?></td>			
			<td><?php echo $cur->tag_h6; ?></td>
		</tr>
		<tr>
			<td>11</td>
			<td>Tag H6 Keyword</td>
			<td><?php echo $sum->tag_h6_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_h6_match, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_h6_match; ?></td>
		</tr>
		<tr>
			<td>12</td>
			<td>Tag Anchor</td>
			<td><?php echo $sum->tag_anchor; ?></td>
			<td><?php echo number_format((float)$avg->tag_anchor, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_anchor; ?></td>
		</tr>
		<tr>
			<td>13</td>
			<td>Tag Paragraph</td>
			<td><?php echo $sum->tag_paragraph; ?></td>
			<td><?php echo number_format((float)$avg->tag_paragraph, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_paragraph; ?></td>
		</tr>
		<tr>
			<td>14</td>
			<td>Tag Paragraph Keyword</td>
			<td><?php echo $sum->tag_paragraph_match; ?></td>
			<td><?php echo number_format((float)$avg->tag_paragraph_match, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_paragraph_match; ?></td>
		</tr>
		<tr>
			<td>15</td>
			<td>Tag Bold</td>
			<td><?php echo $sum->tag_bold; ?></td>
			<td><?php echo number_format((float)$avg->tag_bold, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_bold; ?></td>
		</tr>
		<tr>
			<td>16</td>
			<td>Tag Italic</td>
			<td><?php echo $sum->tag_italic; ?></td>
			<td><?php echo number_format((float)$avg->tag_italic, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_italic; ?></td>
		</tr>

		<tr>
			<td>17</td>
			<td>Tag Img File</td>
			<td><?php echo $sum->tag_img_file; ?></td>
			<td><?php echo number_format((float)$avg->tag_img_file, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_img_file; ?></td>
		</tr>
		<tr>
			<td>18</td>
			<td>Tag Img ALT</td>
			<td><?php echo $sum->tag_img_alt; ?></td>
			<td><?php echo number_format((float)$avg->tag_img_alt, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_img_alt; ?></td>
		</tr>
		<tr>
			<td>19</td>
			<td>Tag Ordered</td>
			<td><?php echo $sum->tag_ordered; ?></td>
			<td><?php echo number_format((float)$avg->tag_ordered, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_ordered; ?></td>
		</tr>
		<tr>
			<td>20</td>
			<td>Tag Ordered List</td>
			<td><?php echo $sum->tag_ordered_list; ?></td>
			<td><?php echo number_format((float)$avg->tag_ordered_list, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_ordered_list; ?></td>
		</tr>
		<tr>
			<td>21</td>
			<td>Tag Unordered</td>
			<td><?php echo $sum->tag_unordered; ?></td>
			<td><?php echo number_format((float)$avg->tag_ordered_list, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_unordered; ?></td>
		</tr>
		<tr>
			<td>22</td>
			<td>Tag Unordered List</td>
			<td><?php echo $sum->tag_unordered_list; ?></td>
			<td><?php echo number_format((float)$avg->tag_unordered_list, 2, '.', ''); ?></td>	
			<td><?php echo $cur->tag_unordered_list; ?></td>
		</tr>
		<tr>
			<td>23</td>
			<td>Words</td>
			<td><?php echo $sum->words; ?></td>
			<td><?php echo number_format((float)$avg->words, 2, '.', ''); ?></td>	
			<td><?php echo $cur->words; ?></td>
		</tr>
			<td>24</td>
			<td>Words Keywords</td>
			<td><?php echo $sum->words_match; ?></td>
			<td><?php echo number_format((float)$avg->words_match, 2, '.', ''); ?></td>				
			<td><?php echo $cur->words_match; ?></td>
		</tr>



	</tbody>
</table>
</div>