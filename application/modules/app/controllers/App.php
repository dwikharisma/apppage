<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use Sunra\PhpSimple\HtmlDomParser;
class App extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper('file');
        $this->load->model('appmodel');        
    }
	
	public function index()
	{
		$data['view']         = 'home';
		$this->load->view('template', $data);
	}

	public function project(){
		$data['view']         = 'project';
		$this->load->view('template', $data);	
	}

	public function report($page=1){
		$perpage = 15;
		/*menentukan offset.offset sendiri menentukan data yang akan di lewati setiap baris*/
		$limit = ($page - 1) * $perpage;
	    $sql=$this->appmodel->list($limit,$perpage);
	     foreach ($this->config->item('pagination') as $key => $value)
        {
            $config[$key] = $value;
        }
        $result=$this->appmodel->list($limit,$perpage);;

        $config['base_url']     = '/app/report/';
        $config['total_rows']   = $result['nums'];               
        $data['pagination']=$this->pagination->initialize($config)->create_links();  
        $data['data']=$result['data'];
        $data['view']= 'report';
		$this->load->view('template', $data);
	}



	public function action(){
		$modul=$this->input->post('modul');
		$url=$this->input->post('url');
		$keywords=$this->input->post('keywords');
		if($modul=='new'){
			$res=$this->hit($url);		
			if($res==true){
				$id=$this->appmodel->new_project($res['file']);
				/*proses parsing*/
				$parse=$this->parse($res['file'],strtolower($keywords));
				$data['id']	 =$id;
				$data['url']=$url;
				$data['file']=$res['file'];
				$data['parse']=$parse;
				$data['type']=1;
				
				$rs_parse=$this->appmodel->parse($data);
				$out['status']=$rs_parse;
				$out['project']=$id;
				$out['keywords']=$keywords;

			}else{
				$out['status']=false;
			}						
			
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($out));       
	}

	public function compare(){
		$modul=$this->input->post('modul');
		for($i=0;$i<5;$i++){
			$id=$this->input->post('project_id');	
			$url=$this->input->post('url_'.$i);
			$keywords=$this->input->post('project_keywords');	
			$res=$this->hit($url);		
			if($res['status']==true){
			/*proses parsing*/
			$parse=$this->parse($res['file'],strtolower($keywords));
			$data['id']	 =$id;
			$data['url']=$url;
			$data['file']=$res['file'];
			$data['parse']=$parse;
			$data['type']=0;
			$rs_parse=$this->appmodel->parse($data);
			$out['status']=$rs_parse;
			$out['project']=$id;
			$out['keywords']=$keywords;

			}else{
			$out['status']=false;
			}						
		}
		


		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($out));       
	}


	function hit($url){		
		$client = new \GuzzleHttp\Client();		
		$options = [
			'headers' => [
				'User-Agent' =>  'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
				'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
				'Accept-Encoding' => 'gzip'
			]
		];
		$res = $client->request('GET', $url,$options);
		$status=$res->getStatusCode();
		if($status==200){
			$folder=date('Y',time()).'/'.date('m',time());
			if (!is_dir('filebox/'.$folder)) {
				mkdir('./filebox/' . $folder, 0775, TRUE);
			}
			$nm_file=md5(time());
			$file = fopen('filebox/'.$folder.'/'.$nm_file.'.html', 'w');		
			fwrite($file, $res->getBody());		
			$out['status']=true;
			$out['file']=$nm_file;
		}else{
			$out['status']=false;
		}
		return $out;
	}

	public function analyze(){
		$id=$this->input->post('project_id');
		$data['sum']=$this->appmodel->perform_sum($id);
		$data['avg']=$this->appmodel->perform_avg($id);
		$data['cur']=$this->appmodel->project($id);		 		
		$data['pro']=$this->appmodel->project_dt($id);		 		
		$out['status']  =true;
        $out['html']    =$this->load->view('app/result',$data,TRUE);
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($out));  
	}
	public function parse($project,$keywords){
		
		$folder = date('Y',time()).'/'.date('m',time());
		$file  	= 'filebox/'.$folder.'/'.$project.'.html';
		$html 	= HtmlDomParser::file_get_html( $file );
		$arr=array();
		/*tag h*/
		for($i=1;$i<7;$i++){
			${'tag_h'.$i}=array(); 
			foreach($html->find('h'.$i) as $row) {				
				${'tag_h'.$i}[]=substr_count(strtolower($row->plaintext),$keywords);
				${'tag_h'.$i.'_str'}[]=(!is_object($row->plaintext) ? $row->plaintext: 0);
			}

			$arr['tag_h'.$i]=count(${'tag_h'.$i});
			$arr['tag_h'.$i.'_str']=(!empty(${'tag_h'.$i.'_str'}) ? ${'tag_h'.$i.'_str'} : '-');
			$arr['tag_h'.$i.'_match']=${'tag_h'.$i};
			$arr['tag_h'.$i.'_match_nums']=array_sum(${'tag_h'.$i});
		}
		
		/*tag a*/
		foreach($html->find('a') as $row) {
			$tag_a[] 	=(!is_object($row) ? 0 : $row->href);		
		}
			$arr['tag_a']=(isset($tag_a)==true ? count($tag_a) : 0);

		/*tag p*/
		foreach($html->find('p') as $row) {
			$tag_p[]=$row->plaintext;
			$tag_p_match[]=substr_count(strtolower($row->plaintext),$keywords);
			$ex=explode(' ',$row->plaintext);
			$tag_words[]=count($ex);			
		}

		$arr['tag_p']=(!empty($tag_p)==true ? count($tag_p) : 0);
		//$arr['tag_str']=(!empty($tag_p) ? $tag_p : 0);
		$arr['tag_p_match']=(!empty($tag_p_match) ? $tag_p_match : 0);
		$arr['tag_p_match_nums']=(!empty($tag_p_match) ? array_sum($tag_p_match) : 0);
		$arr['tag_p_words']=(!empty($tag_words) ? array_sum($tag_words) : 0);

		/*tag b*/
		foreach($html->find('b') as $row) {
			$tag_b[]=$row->plaintext;			
		}

		$arr['tag_b']=(isset($tag_b)==true ? count($tag_b) : 0);
		/*tag i*/
		foreach($html->find('i') as $row) {
			$tag_i[]=(!empty($row->plaintext) ? $row->plaintext : 0);
		}
		$arr['tag_i']=(isset($tag_i)==true ? count($tag_i) : 0);

		/*tag img*/
		foreach($html->find('img') as $row) {
			$tag_img[]=(!empty($row->src) ? $row->src : 0);
			if(!empty($row->alt)){
				$tag_img_alt[]=$row->alt;	
			}			
		}

		$arr['tag_img_src_num']=(!empty($tag_img) ? count($tag_img) : 0);
		$arr['tag_img_alt_num']=(!empty($tag_img_alt) ? count($tag_img_alt) : 0);
		$arr['tag_img_src']=(!empty($tag_img) ? $tag_img : '-');
		$arr['tag_img_alt']=(!empty($tag_img_alt) ? $tag_img_alt : '-');		

		foreach($html->find('ol') as $row) {
			$tag_ol[]=(!empty($row->plaintext) ? $row->plaintext : 0);								
		}

		$arr['tag_ol']		=(!empty($tag_ol) ? $tag_ol: 0);	
		$arr['tag_ol_num']	=(!empty($tag_ol) ? count($tag_ol): 0);	;	

		foreach($html->find('ol li') as $row) {
			$tag_ol_li[]=(!empty($row->plaintext) ? $row->plaintext: 0);								
		}

		$arr['tag_ol_li']=(!empty($tag_ol_li) ? $tag_ol_li: 0);
		$arr['tag_ol_li_num']=(!empty($tag_ol_li) ? count($tag_ol_li): 0);;	

		foreach($html->find('ul') as $row) {
			$tag_ul[]=(!empty($row->plaintext) ? $row->plaintext: 0);								
		}
		$arr['tag_ul']=(!empty($tag_ul) ? $tag_ul: 0);
		$arr['tag_ul_num']=(!empty($tag_ul) ? count($tag_ul): 0);	

		foreach($html->find('ul li') as $row) {
			$tag_ul_li[]=(!empty($row->plaintext) ? $row->plaintext: 0);								
		}

		$arr['tag_ul_li'] =(!empty($tag_ul_li) ? $tag_ul_li: 0);
		$arr['tag_ul_li_num']=(!empty($tag_ul_li) ? count($tag_ul_li): 0);

		foreach($html->find('iframe') as $row) {
			$tag_iframe[]=(!empty($row->src) ? $row->src: 0);
		}

		$arr['tag_iframe']=(!empty($tag_iframe) ? $tag_iframe: 0);	
		$arr['tag_iframe_num']=(!empty($tag_iframe) ? count($tag_iframe): 0);
		$push=array();
		foreach($html->find('text') as $key=>$row) {			
			if (!ctype_space($row->innertext)) {
				foreach($html->find('script,style') as $val) {
					if(trim(strtolower($row->innertext)) !== trim(strtolower($val->innertext))){						
						//$tag_word=str_word_count($row->innertext,2);
						$txt[$key]=$row->innertext;
									
					}else{
						$txt_txt[$key]=$row->innertext;
						$push[]=$key;	
					}					
				}	
			}			
		}

		$arr['push']=array_unique($push);		
		$arr['tag_txt']=(!empty($txt) ? $txt : '-');		
		foreach ($push as $key => $value) {
			unset($txt[$value]);
		}

		if(!empty($txt)){
			foreach ($txt as $key => $words) {
				$words_match[]=substr_count(strtolower($words),$keywords);
				$words_sum[]=count(explode(' ',$words));	
			}
		}

			$arr['txt_words']=(!empty($txt) ? $txt : '-');;
			$arr['txt_words_nums']=(!empty($words_sum) ?array_sum($words_sum) : 0 );
			$arr['txt_words_match']=(!empty($words_match) ?array_sum($words_match) : 0 );

		foreach($html->find('script') as $row) {
				$txt_script[]=$row->innertext;										
		}

		$arr['txt_script']=(!empty($txt_script) ? $txt_script : 0);
		$arr['txt_script_nums']=(!empty($txt_script) ? count($txt_script) : 0);
		
		foreach($html->find('style') as $row) {
				$txt_style[]=(!empty($row->innertext) ? $row->innertext: 0);							
		}

		$arr['txt_style']=(!empty($txt_style) ? $txt_style : 0);
		$arr['txt_style_nums']=(!empty($txt_style) ? count($txt_style) : 0);
		//print_r($arr);
		return $arr;
	}

	function group_by($key, $data) {
    $result = array();

    foreach($array as $val) {
        if(array_key_exists($key, $val)){
            $result[$val[$key]][] = $val;
        }else{
            $result[""][] = $val;
        }
    }

    return $result;
}

	public function page(){
		$client = new Client();

//$crawler = $client->request('GET', 'https://www.symfony.com/blog/');
		$client->setHeader('User-Agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
		$crawler=$client->request('GET', 'https://agent.sriwijayaair.co.id/SJ-Eticket/login.php?action=in');
		var_dump($crawler);
	}
}

