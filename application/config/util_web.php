<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['pagination']	= [
    'full_tag_open'         => '<div class="pagging text-center"><nav><ul class="pagination">',
    'full_tag_close'        => '</ul></nav></div>',

    'num_tag_open'          => '<li class="page-item"><span class="page-link">',
    'num_tag_close'         => '</span></li>',

    'first_link'            => false,
    'last_link'             => false,
    'first_tag_open'		=> '<li class="page-item"><span class="page-link">',
    'first_tag_close'		=> '</span></li>',
    
    'prev_link'             => 'Previous',
    'prev_tag_open'         => '<li class="page-item"><span class="page-link">',
    'prev_tag_close'        => '</span></li>',

    'next_link'             => 'Next',
    'next_tag_open'         => '<li class="page-item"><span class="page-link">',
    'next_tag_close'        => '<span aria-hidden="true">&raquo;</span></span></li>',

    'last_tag_open'         => '<li class="page-item"><span class="page-link">',
    'last_tag_close'        => '</span></li>',

    
    

    'cur_tag_open'          => '<li class="page-item active"><span class="page-link">',
    'cur_tag_close'         => '<span class="sr-only">(current)</span></span></li>',
	
    'use_page_numbers'      => TRUE,
    // 'first_url'				=>0,
    'per_page'				=>15		
];