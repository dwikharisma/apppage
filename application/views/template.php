<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$class = strtolower($this->uri->segment(1));
$this->load->view('header');
$this->load->view('left');
$this->load->view('right');
if (!empty($class))
    $class = $class;
else
    $class = 'app';

$this->load->view($class . '/' . $view);
//$this->load->view('footer');

?>